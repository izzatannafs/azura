class Produk:
    def __init__(self, nm, npr):
        self.nama = str(nm)
        self.nop = str(npr)

    def getNama(self):
       return self.nama

    def getNop(self):
        return self.nop

    def setNama(self, nm):
        self.nama = nm

    def setNop(self, npr):
        self.nop = npr

DftrPrd = {}
loop = True

print("===================================")
print("=         Daftar Produk           =")
print("===================================")
print("= # MENU                          =")
print("= 1. Tambah Produk                =")
print("= 2. Hapus Produk                 =")
print("= 3. Tampilkan Semua Produk       =")
print("= 4. Cari Produk                  =")
print("= 5. Edit Nama Produk             =")
print("= 6. Edit Kode Produk             =")
print("= 7. Jumlah Total Produk          =")
print("= 8. Keluar                       =")
print("===================================")

while(loop):
    print("\n\n")
    menu = int(input("Masukan menu : "))

    if menu == 1:
        print("\n")
        nama = str(input("Masukan Nama Produk : "))
        nop = str(input("Masukan Kode Produk : "))
        prd = Produk(nama, nop)
        DftrPrd[nop] = prd
    
    elif menu == 2:
         nop = str(input("Masukan Kode Produk : "))
         if(nop in DftrPrd):
             del DftrPrd[nop]
         else:
            print("Data tidak ditemukan")
       
    elif menu == 3:
         print("\n")
         for i in DftrPrd:
            print("Nama Produk :", DftrPrd[i].getNama())
            print("Kode Produk :", DftrPrd[i].getNop())
            print("\n")
        
    elif menu == 4:
        nop = str(input("Masukan Kode Produk : "))
        if(nop in DftrPrd):
            print("Nama Produk : ", DftrPrd[nop].getNama())
            print("Kode Produk : ", DftrPrd[nop].getNop())
        else:
            print("Data tidak ditemukan")
       
    elif menu == 5:
        nop = str(input("Masukan Kode Produk : "))
        if(nop in DftrPrd):
            namaBaru = str(input("Masukan Nama Baru : "))
            DftrPrd[nop].setNama(namaBaru)
        else:
            print("Data tidak ditemukan")
       
    elif menu == 6:
        nop = str(input("Masukan Kode Produk : "))
        if(nop in DftrPrd):
            nopBaru = str(input("Masukan Kode Produk Baru : "))
            DftrPrd[nop].setNop(nopBaru)
            prd = DftrPrd[nop]
            DftrPrd[nopBaru] = prd
            del DftrPrd[nop]
        else:
            print("Data tidak ditemukan")
       
    elif menu == 7:
        print("Jumlah Total Produk : ", len(DftrPrd))
    
    elif menu == 8:
        loop = False
    
    else:
        print("Not Found")