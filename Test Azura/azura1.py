azura = [
    {'Nama': 'Indomie', 'Harga': 3000, 'rating': 5, 'likes': 150},
    {'Nama': 'Laptop', 'Harga': 4000000, 'rating': 4.5, 'likes': 123},
    {'Nama': 'Aqua', 'Harga': 3000, 'rating': 4, 'likes': 250},
    {'Nama': 'Smart TV', 'Harga': 4000000, 'rating': 4.5, 'likes': 42},
    {'Nama': 'Headphone', 'Harga': 4000000, 'rating': 3.5, 'likes': 90},
    {'Nama': 'Very Smart TV', 'Harga': 4000000, 'rating': 3.5, 'likes': 87},
]

# sorting harga terendah
azura.sort(key=lambda x: x.get('Harga'))
print(azura, end='\n\n')

# sorting dengan rating
azura.sort(key=lambda x: x.get('rating'), reverse=True)
print(azura, end='\n\n')

# sorting dengan like
azura.sort(key=lambda x: x.get('likes'), reverse=True)
print(azura, end='\n\n')